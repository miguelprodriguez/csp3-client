module.exports = {
	// API_URL: `https://budget-tracker-cs3.herokuapp.com/api`,
	API_URL: `http://localhost:4000/api`,
	getAccessToken: () => localStorage.getItem('token'),
	toJSON: (response) => response.json() 
	// process.env. is injected by the NODE at runtime for the application to use and it represents the state of the system environment you application is in when it starts.
}