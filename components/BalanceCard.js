import React, { useState, useEffect } from 'react';

// Bootstrap 
import { Jumbotron } from 'react-bootstrap';

// Helpers
import numberWithCommas from '../helpers/format';

// AppHelper
import AppHelper from '../app-helper'

export default function BalanceCard() {
   
    const [ transactionsData, setTransactionsData ] = useState([])

	useEffect(() => {
		const token = localStorage.getItem('token')
		const options = { headers: {'Authorization': `Bearer ${token}`} }
		fetch(`${ AppHelper.API_URL }/users/details`, options)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setTransactionsData(data.transactions)
		})
	}, [])

    	// FUNCTION FOR INCOME/EXPENSE CARDS 
  	const incomeExpensesCard = transactionsData.map(transaction => {
        const amounts = transaction.amount
        return transaction.amount
    })
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Reduce_of_empty_array_with_no_initial_value
    const income = incomeExpensesCard.filter(item => item > 0)
    const incomeTotal = income.reduce((a, b) => a + b, 0)
    const incomeTotalCard = numberWithCommas(income.reduce((a, b) => a + b, 0).toFixed(2)) 
    // toFixed returns a string
    
    const expenses = incomeExpensesCard.filter(item => item < 0)
    const expensesTotal = expenses.reduce((a, b) => a + b, 0)
    const expensesTotalCard = numberWithCommas(expenses.reduce((a, b) => a + b, 0).toFixed(2)) 

    const balance = Math.round((incomeTotal + expensesTotal) * 100) / 100
    const balanceFormatted = numberWithCommas(balance.toFixed(2))

    return(
            <React.Fragment>
                <Jumbotron className={(balance > 0) ? "incomeTotal-background" : "expensesTotal-background"}>
                    <h1>Balance:</h1> 
                    <h1>&#8369; {balanceFormatted}</h1>
                </Jumbotron>

                <div className="inc-exp-container">
                    <div className="incomeTotal">
                        <h1 className="mr-1" >Income Total</h1> 
                        <p>&#8369; {incomeTotalCard}</p>	
                    </div>
                    <div className="ml-2 expensesTotal">
                        <h1>Expense Total</h1>
                        <p>&#8369; {expensesTotalCard}</p>
                    </div>
                </div>
            </React.Fragment>
    )
}