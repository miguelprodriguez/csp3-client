import { Doughnut } from 'react-chartjs-2'

export default function DoughnutChart({ data }) {
	return(
		// REFERENCE: https://www.chartjs.org/docs/latest/charts/doughnut.html
		<Doughnut data={{
			datasets: [{
				data: [ ...data ], 
				backgroundColor: [ "green", "red" ]
			}], 
			labels: [ "Income", "Expense" ]
		}} responsive={true} />
		)
}