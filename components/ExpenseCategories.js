import { Pie } from 'react-chartjs-2'

export default function PieChart({ data, label }) {
	return(
		// REFERENCE: https://www.chartjs.org/docs/latest/charts/Pie.html
		<Pie data={{
			datasets: [{
				data: [ ...data ], 
				backgroundColor: [ "pink", "brown", "gold", "black", "pink", "yellow", "gray", "teal" ]
			}], 
			labels: [ ...label ]
		}} responsive={true} />
		)
}