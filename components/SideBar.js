import React, { useState, useContext } from 'react';
import Link from 'next/link';
import UserContext from '../User.Context'

// React Icons BsGear for settings 
import { AiOutlineLogin, AiOutlineLogout, AiOutlineLeft, AiOutlineHome, AiFillFileAdd, AiFillProfile } from "react-icons/ai";
import { BsNewspaper, BsPeopleCircle } from "react-icons/bs";
import { HiOutlineMenuAlt1 } from "react-icons/hi";
import { IoMdAnalytics } from "react-icons/io";
import { GiReceiveMoney } from "react-icons/gi";
import { IconContext } from "react-icons";

import Cookies from 'js-cookie'

export default function Sidebar() {

	const { user } = useContext(UserContext)
	const [sidebar, setSidebar] = useState(false);

	const showSidebar = () => {
		setSidebar(!sidebar);
	}

	return( 
		<React.Fragment>
			<IconContext.Provider value={{ color: "white" }}>  {/* change values of the icons */} 
				<div className='navbar'>
						<Link href='#'>
							<a className='menu-bars'>
								<HiOutlineMenuAlt1 onClick={showSidebar}/>
							</a>
						</Link>
					<div className='navbar-brand'>
						<Link href="/" > 
							<a className='navbar-brand'>
								<GiReceiveMoney /> 
								<span>Money Tracker</span>
							</a>
						</Link>
					</div>
				</div>

				<nav className={sidebar ? "nav-menu active" : "nav-menu"}>
					<ul className="nav-menu-items">
						<li className="navbar-toggle">
							<Link href='#' className="px-5">
								<a className='menu-bars' onClick={showSidebar}>
									<AiOutlineLeft />
								</a>
							</Link>
						</li>
						<li className="nav-text" onClick={showSidebar}>
							<Link href='/' onClick={showSidebar}>
								<a className='menu-bars'>
									<AiOutlineHome />
									<span>Home</span>
								</a>
							</Link>
						</li>

					{/* CONTROL STRUCTURE IF USER IS LOGGED IN */}
					{(Cookies.get('token') != undefined) 
						?
						<React.Fragment>
							<li className="nav-text" onClick={showSidebar}>
								<Link href="/transactions">
									<a className='menu-bars'>
										<BsNewspaper />
										<span>Transactions</span>
									</a>
								</Link>
							</li>
							<li className="nav-text" onClick={showSidebar}>
								<Link href="/categories">
									<a className='menu-bars'>
										<AiFillProfile />
										<span>Categories</span>
									</a>
								</Link>
							</li>
							<li className="nav-text" onClick={showSidebar}>
								<Link href="/analytics">
									<a className='menu-bars'>
										<IoMdAnalytics />
										<span>Breakdown</span>
									</a>
								</Link>
							</li>
							<li className="nav-text" onClick={showSidebar}>
								<Link href="/addTransaction">
									<a className='menu-bars'>
										<AiFillFileAdd />
										<span>Add/Post</span>
									</a>
								</Link>
							</li>
							<li className="nav-text" onClick={showSidebar}>
								<Link href="/logout">
									<a className='menu-bars'>
										<AiOutlineLogout />
										<span>Logout</span>
									</a>
								</Link>
							</li>
						</React.Fragment>
						:
						<React.Fragment>
							<li className="nav-text" onClick={showSidebar}>
								<Link href="/login">
									<a className='menu-bars'>
										<BsPeopleCircle />
										<span>Login</span>
									</a>
								</Link>
							</li>
							
							<li className="nav-text" onClick={showSidebar}>
								<Link href="/register">
									<a className='menu-bars'>
										<AiOutlineLogin />
										<span>Sign Up</span>
									</a>
								</Link>
							</li>
						</React.Fragment>
					}					
					</ul>
				</nav>
			</IconContext.Provider>
		</React.Fragment>
		)
}

