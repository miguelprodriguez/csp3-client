import React from 'react'
import { Button } from 'react-bootstrap'
import { BsTrash } from "react-icons/bs";

const TableData = ({
    id, name, type, remove
}) => {
    return (
        <tr key={id}>
            <td>{name}</td>
            <td>{type}</td>
            <td>
                <Button
                    className='bg-danger ml-2'
                    onClick={() => { remove(id) }}
                >
                    <BsTrash />
                </Button>
            </td>
        </tr>
    )
}

export default TableData