import { Bar } from 'react-chartjs-2'

export default function BarChart({ data, date }) {
	return(
		// REFERENCE: https://www.chartjs.org/docs/latest/charts/Bar.html
		<Bar data={{
			 labels: [ ...date ],
			  datasets: [{
			      label: 'Daily Balance',
			      backgroundColor: "blue",
			      borderWidth: 2,
			      data: [ ...data ]
			    }]
		}} responsive={true} />
		)
}