import React, { useState, useEffect } from 'react'
import '../styles/globals.css'
import '../styles/landing.css'
import '../styles/loginRegister.css'
import '../styles/nav.css'
import '../styles/transactions.css'
import 'bootstrap/dist/css/bootstrap.min.css';

import Cookies from 'js-cookie'

// Components
import SideBar from '../components/SideBar'

// Context
import { UserProvider } from '../User.Context';

function MyApp({ Component, pageProps }) {

	const [ user, setUser ] = useState({ email: null })

	// localStorage can only be accessed after this component has been rendered, hence the NEED forn an effect hook
	useEffect(() => {
		setUser({ email: localStorage.getItem('email') })
	}, [])

	// For logging out
	const unsetUser = () => {
		localStorage.clear();
		// reset also the state hook
		setUser({ email: null })
		Cookies.remove('token')
	}

	return (
		<React.Fragment>
			<UserProvider value={{ user, setUser, unsetUser }}>
				<SideBar />	
		  		<Component {...pageProps} />	
			</UserProvider>	  
	  	</React.Fragment>
	  	)
}

export default MyApp
