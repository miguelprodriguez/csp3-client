import React, { useState, useEffect, useRef } from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'

import Router from 'next/router'
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2'

import { BsPlusCircle } from "react-icons/bs";
	
export default function AddTransaction() {

	// STATE HOOKS FOR THE FORM
	const [ transactionName, setTransactionName ] = useState('')
	const [ transactionAmount, setTransactionAmount ] = useState('')
	const [ transactionType, setTransactionType ] = useState('')
	const [ transactionCategory, setTransactionCategory ] = useState('')

	console.log(transactionCategory)
  	// REDIRECT TO ADD CATEGORY
	function add(e) {
		e.preventDefault();
		Router.push('/categories')
	}

	// ADD TRANSACTION TO TRANSACTIONS ARRAY OF USER MODEL
	function post(e) {
		e.preventDefault()
		setTransactionName('')
		setTransactionAmount('')

		let token = localStorage.getItem('token')	
		
		if(transactionType === "Income") {
			const options = { 
				method: 'POST', 
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({ 
					name: transactionName,
					amount: transactionAmount,
					type: transactionType,
					category: transactionCategory,
				})
			}	

			fetch(`${ AppHelper.API_URL }/users/addTransaction`, options)
	        .then(AppHelper.toJSON)
	        .then(data => {
	            if(data === true) {
	            	Swal.fire({
							  icon: 'success',
							  title: 'Transaction has been added successfully!'
							})
	            	Router.push('/transactions') 
	            } else {
	            	Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'Something went wrong. Please try again.'
					})
	            }
	        })
		} else {
			const options = { 
				method: 'POST', 
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({ 
					name: transactionName,
					amount: (transactionAmount * -1),
					type: transactionType,
					category: transactionCategory,
				})
			}	

			fetch(`${ AppHelper.API_URL }/users/addTransaction`, options)
	        .then(AppHelper.toJSON)
	        .then(data => {
	            if(data === true) {
	            	Swal.fire({
							  icon: 'success',
							  title: 'Transaction has been added successfully!',
							  text: 'You may now check this on your transaction history'
							})
	            	Router.push('/transactions') 
	            } else {
	            	Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'Something went wrong. Please try again.'
					})
	            }
	        })
		}
	}

	// GET CATEGORIES OF USER
	const [ categoriesData, setCategoriesData ] = useState([])
	const isFirstRun = useRef(true);

	useEffect(() => {
		const token = localStorage.getItem('token')
		const options = { headers: {'Authorization': `Bearer ${token}`} }
		fetch(`${ AppHelper.API_URL }/users/details`, options)
		.then(res => res.json())
		.then(data => {
			setCategoriesData(data.categories)
		})
	}, [])

	useEffect (() => {
	    if (isFirstRun.current) {
	      isFirstRun.current = false;
	      return;
	    }
		if(categoriesData.length < 1) {
			Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'You do not have categories. Please add one for income/expense.'
				})
		}
	  });

	const categories = categoriesData.map(transaction => transaction)	
	const incomeCategories = categories.filter(category => category.type == "Income")
	const expenseCategories = categories.filter(category => category.type == "Expense")

	const incomeDropdown = incomeCategories.map(category => {
		return(
			<React.Fragment>
				<option key={category._id} hidden></option>
				<option>{category.name}</option>
			</React.Fragment>
		)
	})
	const expenseDropdown = expenseCategories.map(category => {
		return(
			<React.Fragment>
				<option key={category._id} hidden></option>
				<option>{category.name}</option>
			</React.Fragment>
		)
	})

	return(
		<React.Fragment>

		<title>Add Transaction</title>

			<Container>

			  	<Row className="justify-content-center">	
					<Col md={6}>
						<Form onSubmit={post}>
					  		<h3 className="mt-5">Add Transaction</h3>
							<hr />

							{/* Add category button */}
							<div className="loginButton"> 
				          		<Button variant="success" type="submit" block onClick={add}><strong><BsPlusCircle /> ADD CATEGORY </strong></Button>
				          	</div>

				          	<br />

					  		{/*Transaction Name*/}
					  		<Form.Group className="float-label" controlId="transactionName">
							  	<Form.Control type="text" value={transactionName} onChange={e => setTransactionName(e.target.value)} autoComplete="off" required />
							  	<Form.Label className={transactionName ? "Active" : ""}> Transaction Name </Form.Label>
							</Form.Group>

							{/*Transaction Amount*/}
							<Form.Group className="float-label" controlId="transactionAmount">
							  <Form.Control type="number" value={transactionAmount} onChange={e => setTransactionAmount(e.target.value)} autoComplete="off" required />
				 			  <Form.Label className={transactionAmount ? "Active" : ""}> Transaction Amount </Form.Label>
							</Form.Group>

							{/*Transaction Type*/}
							<Form.Group controlId="transactionType">
							    <Form.Label className="text-muted">Transaction Type</Form.Label>
							    <Form.Control className="dropdown" as="select" value={transactionType} onChange={e => setTransactionType(e.target.value)} autoComplete="off" required >
							      <option hidden></option>
							      <option>Income</option>
							      <option>Expense</option>
								</Form.Control>
							</Form.Group>

							{/*Transaction Category*/}
							{transactionType == "" 
								?
								<Form.Group controlId="transactionCategory">
									<Form.Label className="text-muted">Transaction Category</Form.Label>
									<Form.Control className="dropdown" as="select" value={transactionCategory} 
										onChange={e => setTransactionCategory(e.target.value)} 
										autoComplete="off" required>
										<option></option>
									</Form.Control>
								</Form.Group>
								:
								(transactionType == "Income")
									?
									<Form.Group controlId="transactionCategory">
										<Form.Label className="text-muted">Transaction Category</Form.Label>
										<Form.Control className="dropdown" as="select" value={transactionCategory} 
											onChange={e => setTransactionCategory(e.target.value)} 
											autoComplete="off" required>
											{incomeDropdown}
										</Form.Control>
									</Form.Group>
									:
									<Form.Group controlId="transactionCategory">
										<Form.Label className="text-muted">Transaction Category</Form.Label>
										<Form.Control className="dropdown" as="select" value={transactionCategory} 
											onChange={e => setTransactionCategory(e.target.value)} 
											autoComplete="off" required>
											{expenseDropdown}
										</Form.Control>
									</Form.Group>
							}
						
							{/*Submit Button*/}
							<div className="loginButton">
								<Button variant="primary" type="submit" block><strong>POST TRANSACTION</strong></Button>
							</div>

						</Form>
					
					</Col>
				</Row>
			</Container>
		</React.Fragment>
		)
}