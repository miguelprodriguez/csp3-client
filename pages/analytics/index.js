import React, { useState, useEffect } from 'react'
import { Container, Jumbotron, Row, Col } from 'react-bootstrap'

import Breakdown from '../../components/Breakdown'
import IncomeCategories from '../../components/IncomeCategories'
import ExpenseCategories from '../../components/ExpenseCategories'
import Timeline from '../../components/Timeline'
import AppHelper from '../../app-helper'
import numberWithCommas from '../../helpers/format'

export default function Top() {
	const [ transactionsData, setTransactionsData ] = useState([])

	useEffect(() => {
		const token = localStorage.getItem('token')
		const options = { headers: {'Authorization': `Bearer ${token}`} }
		fetch(`${ AppHelper.API_URL }/users/details`, options)
		.then(res => res.json())
		.then(data => {
			setTransactionsData(data.transactions)
		})
	}, [])

	// INCOME CATEGORIES
	const holder = {};

	const positiveAmount = transactionsData.filter(transaction => transaction.amount > 0)

	positiveAmount.forEach(function(d) {
	  if (holder.hasOwnProperty(d.category)) {
	    holder[d.category] = holder[d.category] + d.amount;
	  } else {
	    holder[d.category] = d.amount;
	  }
	});

	const categoriesData = [];

	for (const prop in holder) {
	  categoriesData.push({ name: prop, amount: holder[prop] });
	}

	const categoryLabel = categoriesData.map(label => label.name)
	const categorySum = categoriesData.map(label => label.amount)

	// EXPENSE CATEGORIES
	const payload = {};

	const negativeAmount = transactionsData.filter(transaction => transaction.amount < 0)

	negativeAmount.forEach(function(d) {
	  if (payload.hasOwnProperty(d.category)) {
	    payload[d.category] = payload[d.category] + d.amount;
	  } else {
	    payload[d.category] = d.amount;
	  }
	});

	const negativaCategories = [];

	for (const prop in payload) {
	  negativaCategories.push({ name: prop, amount: payload[prop] });
	}

	const categoryNegaLabel = negativaCategories.map(label => label.name)
	const categoryDifference = negativaCategories.map(label => label.amount)

	
  	// BALANCE CARD
  	const incomeExpensesCard = transactionsData.map(transaction => {
  		const amounts = transaction.amount
  		return transaction.amount
  	})

  	const income = incomeExpensesCard.filter(item => item > 0)
  	const incomeTotalData = income.reduce((a, b) => a + b, 0)

  	const expenses = incomeExpensesCard.filter(item => item < 0)
  	const expensesTotal = expenses.reduce((a, b) => a + b, 0)
  	const expensesTotalData = (expensesTotal * -1)

  	const balance = Math.round((incomeTotalData + expensesTotal) * 100) / 100
  	const balanceFormatted = numberWithCommas(balance.toFixed(2))

	// GROUP LABEL DATA BY DATE
	const transactionWhole = transactionsData.filter(transaction => transaction)
	const groups = transactionWhole.reduce((groups, transaction) => {
	  const date = transaction.postedOn.split('T')[0];
	  if (!groups[date]) { 
	    groups[date] = [];
	  }
	  groups[date].push(transaction);
	  return groups;
	}, {});


	const groupArrays = Object.keys(groups).map((date) => {
	  return {
	    date,
	    transactions: groups[date]
	  };
	});

	// BAR COMPONENT
	const dates = groupArrays.map(date => date.transactions)
	const amount = dates.map(i => i.map(j => j.amount))
	const sumOfAmount = amount.map(arr => arr.reduce((sum, item) => sum += item, 0));

	const cumulativeSum = (sum => value => sum += value)(0);
	const dailyBalance = sumOfAmount.map(cumulativeSum)
	const dataDate = groupArrays.map(date => date.date)
	
	// DOUGHNUT COMPONENT
	const dataArray = [ incomeTotalData, expensesTotalData ]

	return(
		<React.Fragment>

		<title>Analytics</title>

		 	<br />
			<Container>
			 	<Row>
			 		<Col md={4}>
						<Jumbotron className={(balance > 0) ? "incomeTotal-background-chart" : "expensesTotal-background-chart"}>
							<h1 className="pt-5">Balance:</h1> 
							<h1>&#8369; {balanceFormatted}</h1>
						</Jumbotron>
					</Col>
					<Col md={4}>
						<Jumbotron className="charts">
							<h1>Income Categories</h1>
							<IncomeCategories data={categorySum} label={categoryLabel}/>
						</Jumbotron>
					</Col>
					<Col md={4}>
						<Jumbotron className="charts">
							<h1>Payment Categories</h1>
							<ExpenseCategories data={categoryDifference} label={categoryNegaLabel}/>
						</Jumbotron>
					</Col>
				</Row> 
			</Container>
			<Container>
				<Row>
					<Col>
						<Jumbotron className="charts">
						 	<h1>Breakdown</h1>
							<Breakdown data={dataArray}/>
						</Jumbotron>
					</Col>
					<Col>
						<Jumbotron className="charts">
							<h1>Timeline</h1>
							<Timeline data={dailyBalance} date={dataDate}/>
						</Jumbotron>
					</Col>	
				</Row>
			</Container>
		</React.Fragment>
		)
}

