import React, { useState, useEffect } from 'react'
import AppHelper from '../../app-helper'
import { Container, Button, Table, Jumbotron, Form, Row, Col } from 'react-bootstrap'
import Swal from 'sweetalert2'
import Router from 'next/router'
import TableData from '../../components/TableData';

export default function Categories() {
	// Greeting the user
	const [username, setUsername] = useState('')
	const [categoriesData, setCategories] = useState([])

	useEffect(() => {
		fetchTableData()
	}, [])

	const fetchTableData = () => {
		const token = localStorage.getItem('token')
		const options = { headers: { 'Authorization': `Bearer ${token}` } }
		fetch(`${AppHelper.API_URL}/users/details`, options)
			.then(res => res.json())
			.then(data => {
				setCategories(data.categories)
				setUsername(data.firstName)
			})
	}

	// REDIRECT IF NOT LOGGED IN 
	useEffect(() => {
		const token = localStorage.getItem('token')
		if (token === null) {
			Swal.fire({
				icon: 'error',
				title: 'You must login first'
			})
			Router.push('/login')
		} else {
			Router.push('/categories')
		}
	}, [])

	// FUNCTIONS
	function remove(categoryId) {
		fetch(`${AppHelper.API_URL}/users/removeCategory`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				categoryId: categoryId,
				googleToken: localStorage.getItem('googleToken')
			})
		})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if (data === true) {
					fetchTableData()
					setTransactionName('')
					setTransactionType('')
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Something went wrong. Please try again.'
					})
				}
			})
	}

	const [transactionName, setTransactionName] = useState('')
	const [transactionType, setTransactionType] = useState('')

	// add category
	function addCategory(e) {
		e.preventDefault()

		let token = localStorage.getItem('token')
		const options = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: transactionName,
				type: transactionType
			})
		}

		fetch(`${AppHelper.API_URL}/users/addCategory`, options)
			.then(AppHelper.toJSON)
			.then(data => {
				if (data === true) {
					fetchTableData()
					setTransactionName('')
					setTransactionType('')
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Something went wrong. Please try adding category again.'
					})
				}
			})
	}

	const categoryRows = categoriesData.map(category => {
		return (
			<TableData
				id={category._id}
				name={category.name}
				type={category.type}
				remove={remove}
			/>
		)
	})

	return (
		<React.Fragment>
			<title>Categories Summary</title>
			<Container>
				<br />
				<Jumbotron className='charts greeting'>
					<h1>Here are your categories, {username}</h1>
				</Jumbotron>
				{/* Add Category*/}
				<Jumbotron className="charts justify-content-center">
					<Row>
						<Col md={6}>
							<Form onSubmit={addCategory}>
								<h3>Add Category</h3>
								<hr />
								{/*Category Name*/}
								<Form.Group className="float-label" controlId="transactionName">
									<Form.Control type="text" value={transactionName} onChange={e => setTransactionName(e.target.value)} autoComplete="off" required />
									<Form.Label className={transactionName ? "Active" : ""}> Category Name </Form.Label>
								</Form.Group>

								{/*Category Type*/}
								<Form.Group controlId="transactionType">
									<Form.Label className="text-muted">Transaction Type</Form.Label>
									<Form.Control className="dropdown" as="select" value={transactionType} onChange={e => setTransactionType(e.target.value)} autoComplete="off" required >
										<option hidden></option>
										<option>Income</option>
										<option>Expense</option>
									</Form.Control>
								</Form.Group>

								{/*Submit Button*/}
								<div className="loginButton">
									<Button variant="primary" type="submit" block><strong>ADD CATEGORY</strong></Button>
								</div>
							</Form>
						</Col>
						<Col md={6}>
							<Table className="mt-5" striped hover responsive="sm">
								<thead>
									<tr>
										<th> Category Name </th>
										<th> Type </th>
										<th> Controls </th>
									</tr>
								</thead>
								<tbody>{categoryRows}</tbody>
							</Table>
						</Col>
					</Row>
				</Jumbotron>
			</Container>
		</React.Fragment>
	)
}