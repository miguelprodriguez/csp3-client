import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';

import Link from 'next/link';
import Router from 'next/router';
import Head from 'next/head';

export default function Home() {

  return (
    <React.Fragment>

    <title>Budget Tracker</title>

    <div className="bonfire-img">
      <Container>
          <div md={6} className="vertical-center">
            <h1><strong>Start saving now with MoneyTracker!</strong></h1>
          </div>
      </Container>
    </div>

    </React.Fragment>
    )
}