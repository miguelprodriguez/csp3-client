  import React, { useState, useContext } from 'react';
import { Container, Jumbotron, Row, Col, Button, Form } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';

// Next
import Link from 'next/link';
import Router from 'next/router';

import AppHelper from '../../app-helper';
import Swal from 'sweetalert2'

import UserContext from '../../User.Context'

import Cookies from 'js-cookie'

export default function Login() {

  // CONTEXT
  const { user, setUser } = useContext(UserContext)
  // STATE HOOKS
  const [ email, setEmail ] = useState('');
  const [ password, setPassword ] = useState('');
  const [ tokenId, setTokenId ] = useState();
  
  // FUNCTIONS
  function check(e){
    e.preventDefault();
    
	const options = { 
			method: 'POST', 
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({ email: email, password: password })
		}
        fetch(`${ AppHelper.API_URL }/users/login`, options).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                Cookies.set('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
	}

  // function to retrieve the userDetails
  const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { Authorization: `Bearer ${accessToken}` } 
        }
        fetch(`${ AppHelper.API_URL }/users/details`, options).then(AppHelper.toJSON).then(data => {
            setUser({ id: data._id })
            Router.push('/transactions')
        })
    }

  // Google Authentication
  const authenticateGoogleToken = (response) => {
    localStorage.setItem('googleToken', response.accessToken)
    const payload = { // payload is just another term for options
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ tokenId: response.tokenId })
    }

    fetch (`${ AppHelper.API_URL }/users/verify-google-id-token`, payload)
    .then(AppHelper.toJSON) // toJSON: (response) => response.json() 
    .then(data => {
        if (typeof data.accessToken !== 'undefined'){
            localStorage.setItem('token', data.accessToken)
            Cookies.set('token', data.accessToken)
            retrieveUserDetails(data.accessToken)
            // store access token 
        } else { 
            if (data.error == 'google-auth-error'){ // in case the internet connection gets disrupted (possible reason)
                Swal.fire(
                    'Google Auth Error',
                    'Google authentication procedure failed',
                    'error'
                )
            } else if (data.error === 'login-type-error') { // login type error
                Swal.fire(
                    'Login Type Error',
                    'You may have registered through a different login procedure',
                        'error'
                )
            }
        }
        })
  }

  return (
    <React.Fragment>

    <title>Login</title>

    <br />
    <Container>
      <Jumbotron className="charts"> 
        <Row className="justify-content-center">  
          <Col md={6}>
            <Form onSubmit={check}>
                <h1>Login</h1>
                {/*Google Login*/}        
                <GoogleLogin 
                  theme="dark"
                  clientId="826791092657-f17amktplki0vp2r7sbo2md9grsfk17u.apps.googleusercontent.com"
                  buttonText="CONTINUE WITH GOOGLE"
                  onSuccess={ authenticateGoogleToken }
                              onFailure={ authenticateGoogleToken }
                              cookiePolicy={ 'single_host_origin' } // data will only come from one source. (cookie) 
                              className="w-100 d-flex justify-content-center"
                /> 
                <br />
                <hr />
                <br />
                {/*Email*/}
                <Form.Group className="float-label" controlId="email">
                  <Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} />
                  <Form.Label className={email ? "Active" : ""}> E-mail </Form.Label>
              </Form.Group>

              {/*Password*/}
              <Form.Group className="float-label" controlId="password">
                <Form.Control type="password" value={password} onChange={e => setPassword(e.target.value)} />
                <Form.Label className={password ? "Active" : ""}> Password </Form.Label>
              </Form.Group>
              <div className="loginButton">
              <Button variant="primary" type="submit" block><strong>LOG IN</strong></Button>
              </div>
            </Form>
            {/*<p className="my-4 bottom-text">Forgot your <Link href="/register"><a>password</a></Link>?</p>*/}
            <p className="my-4 bottom-text">New to Money Tracker? <Link href="/register"><a> Sign Up </a></Link> </p>
          </Col>
        </Row>
      </Jumbotron>
    </Container>
    </React.Fragment>
    )
}