import React, { useState, useEffect, useContext } from 'react'
import Router from 'next/router'
import UserContext from '../../User.Context'

export default function Logout() {
	const { unsetUser } = useContext(UserContext)

	useEffect(() => {
		unsetUser();
		Router.push('/login');
	}, [])
	return null;
}