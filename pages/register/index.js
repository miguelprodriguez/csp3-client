import React, { useState } from 'react';
import { Container, Row, Col, Button, Form, Jumbotron } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Link from 'next/link';

import Swal from 'sweetalert2'
import AppHelper from '../../app-helper'

import Router from 'next/router'

export default function Login() {
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	
	function print(e){
		e.preventDefault();
		console.log(email);
		console.log(password1);	
	}

	const authenticateGoogleToken = (response) => {
		    	console.log(response) 
	}
	
	function registerUser(e) {
		e.preventDefault();
		if ((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNo !=='') 
			&& (mobileNo.length === 11) 
			&& (password1 === password2)) {
			// Activity
			const options = { 
				method: 'POST', 
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({ email: email	}) // parameter of email exists on backend controller
			}
	        fetch(`${ AppHelper.API_URL }/users/email-exists`, options)
	        .then(res => {
	        	return res.json()
	        })
	        .then(data => {
	        	// if there are no duplicates, POST on DB
	        	if (data === false) {
	        		fetch(`${ AppHelper.API_URL }/users`, {
	        			method: 'POST', 
	        			headers: {
	        				'Content-Type': 'application/json'
	        			}, 
	        			body: JSON.stringify({ 
	        				firstName: firstName,
	        				lastName: lastName,
	        				email: email,
	        				password: password1,
	        				mobileNo: mobileNo
	        			})
	        		})
	        		.then(res => {
	        			return res.json()
	        		})
	        		.then(data => {
	        			if (data === true) {
							Swal.fire({
							  icon: 'success',
							  title: 'User Successfully Created!',
							  text: 'You may now log in'
							})
							Router.push('/login')
	        			} else {
	        				Swal.fire({
							  icon: 'warning',
							  title: 'Data is not true'
							})
	        			}
	        		})
	        	} else {
	        		Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'You have already registered!'
					})
	        	}
	        })
		} else {
			Swal.fire({
			  icon: 'warning',
			  title: 'There is something wrong with your form'
			})
		}
	}

	return (
		<React.Fragment>

		<title>Register</title>
		
		
		<Container>
			<Jumbotron className="charts">
			  	<Row className="justify-content-center">	
					<Col md={6}>
						<Form onSubmit={registerUser}>
					  		<h3 className="my-5">Sign up</h3>
							{/*Google Login*/}				
							<GoogleLogin 
								theme="dark"
								clientId="826791092657-f17amktplki0vp2r7sbo2md9grsfk17u.apps.googleusercontent.com"
								buttonText="CONTINUE WITH GOOGLE"
								onSuccess={ authenticateGoogleToken }
		                        onFailure={ authenticateGoogleToken }
		                        cookiePolicy={ 'single_host_origin' } // data will only come from one source. (cookie) 
		                        className="w-100 d-flex justify-content-center"
							/> 
								<br />
								<hr />
								<br />
					  		{/*First Name*/}
					  		<Form.Group className="float-label" controlId="firstName">
							  	<Form.Control type="text" value={firstName} onChange={e => setFirstName(e.target.value)} autoComplete="off"/>
							  	<Form.Label className={firstName ? "Active" : ""}> First Name </Form.Label>
							</Form.Group>

							{/*Last Name*/}
					  		<Form.Group className="float-label" controlId="lastName">
							  	<Form.Control type="text" value={lastName} onChange={e => setLastName(e.target.value)} autoComplete="off"/>
							  	<Form.Label className={lastName ? "Active" : ""}> Last Name </Form.Label>
							</Form.Group>

							{/*Email*/}
					  		<Form.Group className="float-label" controlId="email">
							  	<Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} autoComplete="off"/>
							  	<Form.Label className={email ? "Active" : ""}> E-mail </Form.Label>
							</Form.Group>

							{/*Password*/}
							<Form.Group className="float-label" controlId="password1">
							  <Form.Control type="password" value={password1} onChange={e => setPassword1(e.target.value)} autoComplete="off"/>
				 			  <Form.Label className={password1 ? "Active" : ""}> Password </Form.Label>
							</Form.Group>

							{/*Confirm Password*/}
							<Form.Group className="float-label" controlId="password2">
							  <Form.Control type="password" value={password2} onChange={e => setPassword2(e.target.value)} autoComplete="off"/>
				 			  <Form.Label className={password2 ? "Active" : ""}> Confirm Password </Form.Label>
							</Form.Group>

							{/*Mobile Number*/}
							<Form.Group className="float-label" controlId="mobileNo">
							  <Form.Control type="number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} autoComplete="off"/>
				 			  <Form.Label className={mobileNo ? "Active" : ""}> Mobile Number </Form.Label>
							</Form.Group>

							<div className="loginButton">
							<Button variant="primary" type="submit" block><strong>SIGN UP</strong></Button>
							</div>
						</Form>
						<p className="my-5 bottom-text">Already a Money Tracker? <Link href="/login">
							<a>Login</a>
						</Link> </p>
					</Col>
				</Row>
			</Jumbotron>
		</Container>
		</React.Fragment>
		)
}