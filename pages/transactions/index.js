import React, { useState, useContext, useEffect } from 'react'
import AppHelper from '../../app-helper'
import UserContext from '../../User.Context'
import { Container, Button, Table, Jumbotron, Form, Row, Col } from 'react-bootstrap'
import { BsPlusCircle, BsPencil, BsTrash } from "react-icons/bs";

import BalanceCard from '../../components/BalanceCard';

import Swal from 'sweetalert2'
import Router from 'next/router'

import Cookies from 'js-cookie'

export default function Transactions() {
	const { user, setUser } = useContext(UserContext)
	// Greeting the user
	const [username, setUsername] = useState('')

	// REDIRECT IF NOT LOGGED IN 
	useEffect(() => {
		const token = localStorage.getItem('token')
		if (token === null) {
			Swal.fire({
				icon: 'error',
				title: 'You must login first'
			})
			// refresh the page
			Router.push('/login')
		} else {
			Router.push('/transactions')
		}
	}, [])

	// FUNCTIONS
	function add(e) {
		e.preventDefault();
		Router.push('/addTransaction')
	}

	function update(transactionId) {
		console.log(transactionId)
		localStorage.setItem('transactionId', transactionId)
		Router.push('/updateTransaction')
	}

	function remove(transactionId) {
		fetch(`${AppHelper.API_URL}/users/removeTransaction`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				transactionId: transactionId,
				googleToken: localStorage.getItem('googleToken')
			})
		})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if (data === true) {
					Swal.fire({
						icon: 'success',
						title: 'Transaction has been removed successfully!'
					})
					Router.push('/deleteTransaction')
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Oops...',
						text: 'Something went wrong. Please try again.'
					})
				}
			})
	}

	const [transactionsData, setTransactionsData] = useState([])

	useEffect(() => {
		const token = localStorage.getItem('token')
		const options = { headers: { 'Authorization': `Bearer ${token}` } }
		fetch(`${AppHelper.API_URL}/users/details`, options)
			.then(res => res.json())
			.then(data => {
				console.log(data)
				setTransactionsData(data.transactions)
				setUsername(data.firstName)
			})
	}, [])


	// TABLE FOR THE TRANSACTIONS
	const transactionRows = transactionsData.map(transaction => {
		return (
			<tr key={transaction._id}>
				<td>{transaction.postedOn.slice(0, 10)}</td>
				<td>{transaction.name}</td>
				<td>{transaction.type}</td>
				<td>&#8369; {transaction.amount}</td>
				<td>{transaction.category}</td>
				<td>
					<Button className='bg-warning ml-2' onClick={() => { update(transaction._id) }}><BsPencil /></Button>
					<Button className='bg-danger ml-2' onClick={() => { remove(transaction._id) }}><BsTrash /></Button>
				</td>
			</tr>
		)
	})

	// SEARCH 
	const [searchData, setSearchData] = useState([])
	const [targetName, setTargetName] = useState('')

	function search(e) {
		e.preventDefault()
		const match = transactionsData.filter(transaction => transaction.name.toUpperCase().includes(targetName.toUpperCase()))
		if (match) {
			setSearchData(match)
		} else {
			Swal.fire({
				icon: 'error',
				title: 'The transaction name is not present on our database.'
			})
			setSearchData([])
		}
	}

	const searchedRows = searchData.map(transaction => {
		return (
			<tr key={transaction._id} >
				<td>{transaction.postedOn.slice(0, 10)}</td>
				<td>{transaction.name}</td>
				<td>{transaction.type}</td>
				<td>&#8369; {transaction.amount}</td>
				<td>{transaction.category}</td>
				<td>
					<Button className='bg-warning ml-2' onClick={() => { update(transaction._id) }}><BsPencil /></Button>
					<Button className='bg-danger ml-2' onClick={() => { remove(transaction._id) }}><BsTrash /></Button>
				</td>
			</tr>
		)
	})

	// FILTER
	const [filteredDataState, setFilteredDataState] = useState([])
	const [transactionType, setTransactionType] = useState('')

	function filter(e) {
		e.preventDefault()
		const filteredData = transactionsData.filter(transaction => transaction.type.toUpperCase() == transactionType.toUpperCase())
		if (filteredData) {
			setFilteredDataState(filteredData)
		} else {
			setFilteredDataState([])
		}
	}

	const filteredRows = filteredDataState.map(transaction => {
		return (
			<tr key={transaction._id} >
				<td>{transaction.postedOn.slice(0, 10)}</td>
				<td>{transaction.name}</td>
				<td>{transaction.type}</td>
				<td>&#8369; {transaction.amount}</td>
				<td>{transaction.category}</td>
				<td>
					<Button className='bg-warning ml-2' onClick={() => { update(transaction._id) }}><BsPencil /></Button>
					<Button className='bg-danger ml-2' onClick={() => { remove(transaction._id) }}><BsTrash /></Button>
				</td>
			</tr>
		)
	})

	return (
		<React.Fragment>
			<title>Transaction Summary</title>

			<Container>
				{/* Greeting */}
				<br />
				<Jumbotron className='charts greeting'>
					<h1>Welcome, {username}</h1>
				</Jumbotron>
				<br />

				<BalanceCard />

				{/* Add transaction button */}
				<div className="loginButton">
					<Button variant="success" type="submit" block onClick={add}><strong><BsPlusCircle /> ADD TRANSACTION</strong></Button>
				</div>

				<br />

				{/* Search & Filter */}
				<div className="inc-exp-container">
					<Row>
						<Col md={6}>
							<Form onSubmit={search}>
								<Form.Group controlId="searchName">
									<Form.Label className="text-muted"> Search transaction name here </Form.Label>
									<div className="input-group">
										<Form.Control type="text" value={targetName} onChange={e => setTargetName(e.target.value)} autoComplete="off" />
										<Button variant="success" type="submit" className="form-inline"><strong>SEARH</strong></Button>
									</div>
								</Form.Group>
							</Form>
						</Col>

						<Col md={6}>
							<Form onSubmit={filter}>
								<Form.Group controlId="transactionType">
									<Form.Label className="text-muted">Filter by Transaction Type</Form.Label>
									<div className="input-group">
										<Form.Control className="dropdown" as="select" value={transactionType} onChange={e => setTransactionType(e.target.value)}>
											<option>Show All</option>
											<option>Income</option>
											<option>Expense</option>
										</Form.Control>
										<Button variant="success" type="submit" className="form-inline"><strong>FILTER</strong></Button>
									</div>
								</Form.Group>
							</Form>
						</Col>
					</Row>
				</div>

				{targetName.length != 0 ?
					<Table striped hover responsive="sm">
						<thead>
							<tr>
								<th> Date </th>
								<th> Name </th>
								<th> Type </th>
								<th> Amount </th>
								<th> Category </th>
								<th> Controls </th>
							</tr>
						</thead>
						<tbody>{searchedRows}</tbody>
					</Table>
					:
					<Table striped hover responsive="sm">
						<thead>
							<tr>
								<th> Date </th>
								<th> Name </th>
								<th> Type </th>
								<th> Amount </th>
								<th> Category </th>
								<th> Controls </th>
							</tr>
						</thead>
						{filteredDataState.length == 0
							?
							<tbody>{transactionRows}</tbody>
							:
							<tbody>{filteredRows}</tbody>
						}
					</Table>
				}
			</Container>
		</React.Fragment>
	)
}