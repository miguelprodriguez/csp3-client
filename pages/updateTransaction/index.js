import React, { useState, useEffect } from 'react'
import { Container, Row, Col, Form, Button, Jumbotron } from 'react-bootstrap'

import Router from 'next/router'
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2'
	
export default function AddTransaction() {

	const [ transactionsData, setTransactionsData ] = useState([])
	
	const [ transactionName, setTransactionName ] = useState('')
	const [ transactionAmount, setTransactionAmount ] = useState('')
	const [ transactionType, setTransactionType ] = useState('')
	const [ transactionCategory, setTransactionCategory ] = useState('')

	const [ updateName, setUpdateName ] = useState('')
	const [ updateAmount, setUpdateAmount ] = useState('')
	const [ updateType, setUpdateType ] = useState('')
	const [ updateCategory, setUpdateCategory ] = useState('')

	useEffect(() => {
		const token = localStorage.getItem('token')
		const transactionId = localStorage.getItem('transactionId')
		console.log(transactionId)
		const options = { headers: {'Authorization': `Bearer ${token}`} }
		fetch(`${ AppHelper.API_URL }/users/details`, options)
		.then(res => res.json())
		.then(data => {
			const match = data.transactions.find(x => x._id === transactionId)
			console.log(match)
			setUpdateName(match.name)
			setUpdateAmount(match.amount)
			setUpdateType(match.type)
			setUpdateCategory(match.category)
		})
	}, [])

	function update(e) {
		e.preventDefault()
		setTransactionName('')
		setTransactionAmount('')

		let token = localStorage.getItem('token')	
		const transactionId = localStorage.getItem('transactionId')
		
		if(updateType == "Income") {
			const options = { 
				method: 'PUT', 
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({ 
					name: transactionName,
					amount: transactionAmount,
					type: transactionType,
					category: transactionCategory,
					transactionId: transactionId
				})
			}	

			fetch(`${ AppHelper.API_URL }/users/updateTransaction`, options)
	        .then(AppHelper.toJSON)
	        .then(data => {
	            if(data === true) {
	            	Swal.fire({
							  icon: 'success',
							  title: 'Transaction has been updated successfully!'
							})
	            	Router.push('/transactions') 
	            } else {
	            	Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'Something went wrong. Please try again.'
					})
	            }
	        })
		} else {
			const options = { 
				method: 'PUT', 
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({ 
					name: transactionName,
					amount: (transactionAmount * -1),
					type: transactionType,
					category: transactionCategory,
					transactionId: transactionId
				})
			}	

			fetch(`${ AppHelper.API_URL }/users/updateTransaction`, options)
	        .then(AppHelper.toJSON)
	        .then(data => {
	            if(data === true) {
	            	Swal.fire({
							  icon: 'success',
							  title: 'Transaction has been updated successfully!'
							})
	            	Router.push('/transactions') 
	            } else {
	            	Swal.fire({
					  icon: 'error',
					  title: 'Oops...',
					  text: 'Something went wrong. Please try again.'
					})
	            }
	        })
		}
    }

    // GET CATEGORIES OF USER
	const [ categoriesData, setCategoriesData ] = useState([])

	useEffect(() => {
		const token = localStorage.getItem('token')
		const options = { headers: {'Authorization': `Bearer ${token}`} }
		fetch(`${ AppHelper.API_URL }/users/details`, options)
		.then(res => res.json())
		.then(data => {
			setCategoriesData(data.categories)
		})
	}, [])
	console.log(categoriesData)


	const categories = categoriesData.map(transaction => transaction)	
	const incomeCategories = categories.filter(category => category.type == "Income")
	const expenseCategories = categories.filter(category => category.type == "Expense")

	const incomeDropdown = incomeCategories.map(category => {
		return(
			<React.Fragment>
				<option key={category._id} hidden></option>
				<option>{category.name}</option>
			</React.Fragment>
		)
	})
	const expenseDropdown = expenseCategories.map(category => {
		return(
			<React.Fragment>
				<option key={category._id} hidden></option>
				<option>{category.name}</option>
			</React.Fragment>
		)
	})
	return(
		<React.Fragment>

		<title>Update Transaction</title>

			<Container>
			  	<Row className="justify-content-center">	
					<Col md={6}>
						<Form onSubmit={update}>
					  		<h3 className="mt-5">Update Transaction</h3>
							<hr />
					  		{/*Transaction Name*/}
					  		<Form.Group className="float-label" controlId="transactionName">
							  	<Form.Control type="text" value={transactionName} placeholder={updateName} onChange={e => setTransactionName(e.target.value)} autoComplete="off" required />
							  	<Form.Label className="Active"> Transaction Name </Form.Label>
							</Form.Group>

							{/*Transaction Amount*/}
							<Form.Group className="float-label" controlId="transactionAmount">
							  <Form.Control type="number" value={transactionAmount} placeholder={updateAmount} onChange={e => setTransactionAmount(e.target.value)} autoComplete="off" required />
				 			  <Form.Label className="Active"> Transaction Amount </Form.Label>
							</Form.Group>

							{/*Transaction Type*/}
							<Form.Group controlId="transactionType">
							    <Form.Label className="text-muted">Transaction Type</Form.Label>
							    <Form.Control className="dropdown" as="select" value={transactionType} placeholder={updateType} onChange={e => setTransactionType(e.target.value)} autoComplete="off" required >
							      <option hidden></option>
							      <option>Income</option>
							      <option>Expense</option>
								</Form.Control>
							</Form.Group>

							{/*Transaction Category*/}
							{transactionType == "" 
								?
								<Form.Group controlId="transactionCategory">
									<Form.Label className="text-muted">Transaction Category</Form.Label>
									<Form.Control className="dropdown" as="select" value={transactionCategory} 
										onChange={e => setTransactionCategory(e.target.value)} 
										autoComplete="off" required>
										<option></option>
									</Form.Control>
								</Form.Group>
								:
								(transactionType == "Income")
									?
									<Form.Group controlId="transactionCategory">
										<Form.Label className="text-muted">Transaction Category</Form.Label>
										<Form.Control className="dropdown" as="select" value={transactionCategory} 
											onChange={e => setTransactionCategory(e.target.value)} 
											autoComplete="off" required>
											{incomeDropdown}
										</Form.Control>
									</Form.Group>
									:
									<Form.Group controlId="transactionCategory">
										<Form.Label className="text-muted">Transaction Category</Form.Label>
										<Form.Control className="dropdown" as="select" value={transactionCategory} 
											onChange={e => setTransactionCategory(e.target.value)} 
											autoComplete="off" required>
											{expenseDropdown}
										</Form.Control>
									</Form.Group>
							}

							{/*Submit Button*/}
							<div className="loginButton">
								<Button variant="primary" type="submit" block><strong>POST TRANSACTION</strong></Button>
							</div>

						</Form>
					
					</Col>
				</Row>
			</Container>
		</React.Fragment>
		)
}